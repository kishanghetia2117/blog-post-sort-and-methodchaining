# **Chaining of Higher-Order Function in Javascript**

## **What is Higher-Order Functions**

Higher-Order Functions are ones that take another function as an argument.

To understand how the Higher-Order Function work we need to first know a few basic concepts used to create a Higher_order Function.

## 1. Assigning Variables to a Function

Functions in JavaScript can be assigned to a variable just like how we assign data to variables. They differ in one way from other data Types and that is they can be invoked i.e called indirectly by other part's of the code

```js
const printFirstName = (myFirstname) => {
    console.log(`My name is ${myFirstname}`)
}

function printLasttName(myLastName) {
    console.log(`My name is ${myLastName}`)
}

const getLastName = printLasttName;

console.log(printFirstName); 
console.log(getLastName); 
```

```text
// output's 

[Function: printFirstName]
[Function: printLasttName]
```

## 2. Callback Functions

CallBack functions in JavaScript, are those that can be passed to another function as a parameter. The callback function can be later invoked during the execution of the higher-order function to which it was passed as an argument.

This is because javascript treats almost everything as an object including function

```js
let callback = function (n) {
    return n * n * n;
}

const checkForLastDig = (num, cb) => {
    let result
    if (num % 10 === 0) {
        result = cb(num);
    } else {
        result = num;
    }
    console.log(result)
}
//callback function is passed as an argument to checkForLastDig function
checkForLastDig(10, callback);
checkForLastDig(15, callback);
```

```text
// output's 

1000
15
```

## **Higher-Order Functions in Array.protoType**

The JavaScript array prototype constructor is used to allow us to add new methods and properties to the Array() object.

There are many built-in methods. But for this topic, we are going to mainly focus on three higher-order functions:-

1. map()
2. reduce()
3. filter()

## **Example**

We will use the below example that wants us to perform three query

1. Multiple the food cost by 100.
2. Filter the ones who have not paid the bill.
3. Get the name and number of filtered once from the previous query.

Before we continue let us define the data that is needed for the examples

```js
const zomato = [
    {
        "id": 1,
        "name": "bob",
        "food_cost": 12,
        "paid": false,
        "phone_no": 804671620
    },
    {
        "id": 2,
        "name": "luna",
        "food_cost": 15,
        "paid": true,
        "phone_no": 904561620,
    },
    {
        "id": 3,
        "name": "Moon",
        "food_cost": 40,
        "paid": false,
        "phone_no": 944671620,
    }
];
```

## **Solution**

We could use the conventional approach of solving it by loop's but it would be a more complex task to understand as well as build the code with this method, Hence we use HOF to solve the problem which reduces the code size, decreases the number of variables declared and make the code simpler.

## **.map()**

The map() method creates a new array populated with the results of calling a provided function on every element in the calling array.

In the below example we are multiplying the cost by 100 to get the actual cost from the Zomato Dataset.

```js
let query = zomato.map((person) => {
    person.food_cost = person.food_cost * 100;
    return person
})
console.log(query)
```

```txt
output:

[
  {
    id: 1,
    name: 'bob',
    food_cost: 1200,
    paid: false,
    phone_no: 804671620
  },
  {
    id: 2,
    name: 'luna',
    food_cost: 1500,
    paid: true,
    phone_no: 904561620
  },
  {
    id: 3,
    name: 'Moon',
    food_cost: 4000,
    paid: false,
    phone_no: 944671620
  }
]
```

## **.filter()**

The filter() method creates a new array with all elements that pass the test implemented by the provided function.

In the below example we have filtered the dataset to get the data of once who have not yet paid the bill.

```js
let query = zomato.map((person) => {
    person.food_cost = person.food_cost * 100;
    return person
})
console.log(query)
```

```txt
output:

[
  {
    id: 1,
    name: 'bob',
    food_cost: 1200,
    paid: false,
    phone_no: 804671620
  },
  {
    id: 3,
    name: 'Moon',
    food_cost: 4000,
    paid: false,
    phone_no: 944671620
  }
]
```

## **.Reduce()**

The reduce() method executes a user-supplied “reducer” callback function on each element of the array in order and passes in the return value from the calculation on the preceding element. The final result of running the reducer across all elements of the array is a single value.

In the example below we are trying to get the name and number of the people who did not pay the bill which we got from the previous query.

```js
let query = zomato.map((person) => {
    person.food_cost = person.food_cost * 100;
    return person
})
console.log(query)
```

```txt
output:

{
  bob: { phone_no: 804671620, bill: 1200 },
  luna: { phone_no: 904561620, bill: 1500 },
  Moon: { phone_no: 944671620, bill: 4000 }
}
```

## **Incorrect output**

As you can see we are getting the output of names and phone numbers for all the people in the dataset this is because we are not performing the operation on previous outputs received from the dataset. This can be resolved by chaining.

## **Chaining map,reduce,filter**

By Chaining, we will be able to pass the result of one query to another query

```js
let query = zomato.map((person) => {
    person.food_cost = person.food_cost * 100;
    return person
})
.filter((person) => person.paid == false)
.reduce((totalamt, person) => {
    totalamt[person.name] = {};
    totalamt[person.name].phone_no = person.phone_no;
    totalamt[person.name].bill = person.food_cost;
    return totalamt
}, {})

console.log(query)
```

```txt
output:

{
  bob: { phone_no: 804671620, bill: 1200 },
  Moon: { phone_no: 944671620, bill: 4000 }
}
```

## **Chaining more simpler and reliable approach to write code**

We can rewrite the above code in a much more readable way by writing the callback function separately and assigning the function to a variable

```js
let query =(person) => {
    person.food_cost = person.food_cost * 100;
    return person
}

let query2 = (person) => person.paid == false;

let query3 = (totalamt, person) => {
    totalamt[person.name] = {};
    totalamt[person.name].phone_no = person.phone_no;
    totalamt[person.name].bill = person.food_cost;
    return totalamt
}
```

After which we can club the HOF function as shown below by passing the  variable assigned for the callback function's  as an argument to the HOF

```js
let result = zomato
    .map(query)
    .filter(query2)
    .reduce(query3,{});

console.log(result);

```

```txt
output:

{
  bob: { phone_no: 804671620, bill: 1200 },
  luna: { phone_no: 904561620, bill: 1500 },
  Moon: { phone_no: 944671620, bill: 4000 }
}
```
