# **Array.prototype.sort()**

## **What is .sort()**

* The .sort() method sorts the elements of an array using the in-place algorithm and returns the sorted array and not a copy of the array.
* Sorts in ascending order by default.

## **The most basic example**

```js
const list1 = [10, 15, 1500, 5, 22, 7];
list1.sort()
console.log(list1)
```

```txt
Output:

[ 10, 15, 1500, 22, 5, 7 ]
```

## **Different Syntax of sort**

```js
// Without using function's
sort()

// Using arrow function
sort((eleA, eleB) => { /* ... */ } )

// Using compare function (Optional)
sort(compareFunction)

// Using an inline compare function
sort(function compareFunction(eleA, eleB) { /* code */ })
```

## **How dose .sort() work ?**

Without a compareFunction, All the elements except undefined elements are converted to a string and then compared with UTF-16 units order. what does this mean well let us see it with a live example

## 1. For a list of number's

```js
const list1 = [10, 15, 1500, 5, 22, 7];
list1.sort()
console.log(list1)
```

* In the example Above the numbers are converted into strings. And since string 1500 comes before 5 in UTF-16 order we see the following result.

```txt
Output:

[ 10, 15, 1500, 22, 5, 7 ]
```

## 2. For a list of string's

```js
const strList1 = ["FortNite","CsGO","Valorant","csGo","Apex Legends"];
strList1.sort()
console.log(strList1)
```

* In the example Above we see that csGO is sorted After Valorant while CsGO is sorted before Valorant this is because UTF-16 treats Caps and Non-caps differently. Hence we see the following result.

```txt
Output:

[ 'Apex Legends', 'CsGO', 'FortNite', 'Valorant', 'csGo' ]
```

## **How to resolve the UTF-16 Behavior**

we can resolve the UTF-16 Behavior by using an **Optional compareFunction**.

CompareFunction defines the sort order And uses 2 parameters firstEle and secondEle which are the elements to be compared respectively.

| compareFunction(a,b) | sort order |
| -------------------- | ---------- |
| > 0          |sort b before a|
| < 0           |sort a before b|
| === 0             |keeps original |

## 1. Resolve UTF-16 Behavior For a list of number's

Works only if it doesn't contain Infinity and NaN

```js
// sorting using function expressions 
const list1 = [10, 15, 1500, 5, 22, 7];
list1.sort(function (eleA, eleB) {
    return eleA - eleB;
});
console.log(list1);

// sorting using arrow function expressions 
const list2 = [10, 15, 1500, 5, 22, 7];
list1.sort((a, b) => a - b);
console.log(list2);
```

```txt
Output:

[ 5, 7, 10, 15, 22, 1500 ]
[ 10, 15, 1500, 5, 22, 7 ]
```

## 2. Resolve UTF-16 Behavior For a list of string's

resolve string's with uppercase and lowercase

```js
const strList1 = ["FortNite", "CsGO", "Valorant", "csGo", "Apex Legends"];
strList1.sort(function (eleA, eleB) {
    // converts to comman case
    var A = eleA.toUpperCase();
    var B = eleB.toUpperCase();
    
    if (A < B) {
        return -1;
    }
    if (A > B) {
        return 1;
    }

    return 0;
});

console.log(strList1)
```

```txt
Output:

[ 'Apex Legends', 'CsGO', 'csGo', 'FortNite', 'Valorant' ]
```

## **Sorting Array of object's**

This is done by comparing one of their property value's
When object property with the same values are encountered they will remain in the same order as before calling the sort.

```js
const zomato = [
    {"name": "bob","food_cost": 12,"phone_no": 804671620},
    {"name": "luna","food_cost": 15,"phone_no": 904561620,},
    {"name": "Moon","food_cost": 40,"phone_no": 944671620,}
];

zomato.sort(function (eleA, eleB) {
    var a = eleA.name.toUpperCase();
    var b = eleB.name.toUpperCase();
    if (a < b) {
        return -1;
    }
    if (b > a) {
        return 1;
    }
    return 0;
});

console.log(zomato);
```

```txt
Output:

[
  { name: 'Bob', food_cost: 40, phone_no: 944671620 },
  { name: 'Luna', food_cost: 15, phone_no: 904561620 },
  { name: 'Moon', food_cost: 5, phone_no: 890476677 },
  { name: 'Moon', food_cost: 12, phone_no: 804671620 }
]
```

## **How to Sort Date**

new Date(): When called as a constructor, returns a new Date object.

## 1. sort array of date

```js
var date = ['Mar 19 2021 11:55:00 PM', 'Jan 3 2022 01:040:12 AM', 'Feb 30 2021 05:15:15 PM'];
date.sort(function (dateA, dateB) {
    return new Date(dateA) - new Date(dateB);
})

console.log(date)
```

```txt
[
  'Feb 30 2021 05:15:15 PM',
  'Mar 19 2021 11:55:00 PM',
  'Jan 3 2022 01:040:12 AM'
]
```

## 2. sort Object's having a date

```js
const zomato = [
    { "name": "bob", "food_cost": 12, "phone_no": 804671620, "orderTime": "2021-03-05T19:16:34" },
    { "name": "luna", "food_cost": 15, "phone_no": 904561620, "orderTime": "2022-01-07T11:11:34" },
    { "name": "Moon", "food_cost": 40, "phone_no": 944671620, "orderTime": "2020-05-02T15:16:34" }
];

const result = zomato.sort(function (dateA, dateB) {
    let orderDateA = new Date(dateA.orderTime);
    let orderDateB = new Date(dateB.orderTime);
    return orderDateA - orderDateB;
});

console.log(result);
```

```txt
Output:

[
  {
    name: 'Moon',
    food_cost: 40,
    phone_no: 944671620,
    orderTime: '2020-05-02T15:16:34'
  },
  {
    name: 'bob',
    food_cost: 12,
    phone_no: 804671620,
    orderTime: '2021-03-05T19:16:34'
  },
  {
    name: 'luna',
    food_cost: 15,
    phone_no: 904561620,
    orderTime: '2022-01-07T11:11:34'
  }
]
```
